<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;

class Admin extends BaseController
{
    private $tuoteryhmaModel=null;

    function __construct() 
    {
        $this->tuoteryhmaModel = new TuoteryhmaModel();
    }

	public function index()
	{
            
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        $data['otsikko'] = 'Tuoteryhmät';
        echo view('templates/header.php',$data);
        echo view('templates/footer.php',$data);
		echo view('admin/tuoteryhma.php',$data);
    }
    
    public function tallenna() {
        $tuoteryhmaModel = new TuoteryhmaModel();
        if (!$this->validate([
            'nimi' => 'required|max_length[50]'
        ])) {
            echo view('templates/header.php');
            echo view('templates/footer.php');
            echo view('admin/tuoteryhma_lomake.php');
        }
        else {

            $talleta['nimi'] = $this->request->getPost('nimi');
            $tuoteryhmaModel->save($talleta);
            return redirect('admin/index');
        }       
    }
    public function poista($id) {
        // $tuoteryhmaModel = new TuoteryhmaModel();
        $this->tuoteryhmaModel->poista($id);
        return redirect ('admin/index');    
}
	//--------------------------------------------------------------------

}
